# Object Detection Lite

# Dependencies:
- Python 3.6
- opencv-python
- pillow
- tensorflow
- tensorflow-hub

# Usage

## 1. Створення train datasets
 
Example train set [download](https://photos.app.goo.gl/4pypuNGFSvd1xF5s5) save in foulder **dataset/videos/**

## 2. Розкадровка відео за допомогою OpenCV

Запускаємо команду:

```code
python decompose_videos.py 
```
На виході отримуємо папку "dataset/objects" в якій будуть міститися всі наші відео розбиті на кадри і згруповані по папках.

## 3. Навчання моделі на власних datasets
Запускаємо команду:

```bash
python retrain.py
```
Навчаємо модель на нашому datasets.
На виході отримуємо готовий навчений граф, який можна вже використовувати на desktop машинах.


## 4. Перетворення моделі в tflite за допомогою toco

Для того, щоб оптимізувати модель для мобільних пристрїв запускаємо команду:

```bash
IMAGE_SIZE=224
toco \
  --graph_def_file=tmp/output_graph.pb \
  --output_file=tmp/optimized_graph.lite \
  --input_format=TENSORFLOW_GRAPHDEF \
  --output_format=TFLITE \
  --input_shape=1,${IMAGE_SIZE},${IMAGE_SIZE},3 \
  --input_array=input \
  --output_array=final_result \
  --inference_type=FLOAT \
  --input_data_type=FLOAT
```
Отримуємо оптимізований граф в папці **tmp/**

## 5. Запуск моделі на Android

Відкриваємо за допомогою Android Studio **datawiz-demo-app** 

Копіруємо в готовой Android проект свій граф і файл з мітками графа.

```bash
cp tmp/optimized_graph.lite datawiz-demo-app/app/src/main/assets/graph.lite
cp tmp/output_labels.txt datawiz-demo-app/app/src/main/assets/labels.txt
```
**Запускаємо** ![](https://codelabs.developers.google.com/codelabs/tensorflow-for-poets-2-tflite/img/74540ff4e857014c.png)

Готовий apk файл можна скачати **[тут!](https://drive.google.com/open?id=11ZT0vj2_ykUgzn20U9LOeEwdt-c3Btj0)**

